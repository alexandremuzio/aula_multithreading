#ifndef THREAD_SAFE_STACK_H
#define THREAD_SAFE_STACK_H

#include <vector>

struct Value {
    int val;
    Value(int val) : val(val) {}
};

// Thread safe thread implemented as a linked list.
class ThreadSafeStack {
public:
    bool empty();
    void push(Value v);
    Value pop();
    std::vector<Value> contents();
};

#endif