#include <iostream>
#include <thread>
#include <atomic>

int main() {
    // accepts atomic operations
    std::atomic<int> n(0);

    // Creating thread
    std::thread t([&n] (){
        for (int i = 0; i < 500000; i++) {
            n += 1;
        }
    });

    for (int i = 0; i < 500000; i++) {
        n += 1;
    }

    t.join(); 
    std::cout << "n = " << n << std::endl;
}