#include "thread_safe_stack.h"
#include <iostream>
#include <thread>
#include <vector>

bool ThreadSafeStack::empty() {
}

void ThreadSafeStack::push(Value v) {
}

Value ThreadSafeStack::pop() {
}

std::vector<Value> ThreadSafeStack::contents() {
    return std::vector<Value>();
}

int main() {
    ThreadSafeStack stack;

    // Example usage
    std::thread t1([&stack] () {
        for (int i = 1; i <= 1000; i++)
            stack.push(Value(i));
    });

    std::thread t2([&stack] () {
        for (int i = 1; i <= 1000; i++)
            stack.push(Value(-i));
    });

    // Wait for threads to finish
    t1.join();
    t2.join();

    // Show results
    for (auto& e : stack.contents()) {
        std::cout << e.val << " " << std::endl;
    }
}