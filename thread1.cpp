#include <iostream>
#include <thread>
#include <utility>
#include <chrono>

using namespace std;

void celso_funcao() {
    this_thread::sleep_for(chrono::milliseconds(10));

    for (int i = 0; i < 100000; i++) {
        if (i % 10000 == 0) {
                cout << "Executando ordem " << i << endl;
        }
    }
}
int main() {
 
    // Sem threads
    // celso_funcao();
    // cout << "Acabou o mandato do Celso" << endl; 
   
    // Com threads
    thread t(celso_funcao);
    cout << "Acabou o mandato do Celso" << endl;
     t.join();
    // t.detach();
}
