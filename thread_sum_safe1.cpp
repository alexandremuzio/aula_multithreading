#include <iostream>
#include <thread>
#include <mutex>

std::mutex mtx;

int add(int & n, int x) {
    mtx.lock();
    n += x;
    mtx.unlock();
}

int main() {
    // accepts atomic operations
    int n = 0;
    
    // Creating thread
    std::thread t([&n] (){
        for (int i = 0; i < 500000; i++) {
            add(n, 1);
        }
    });

    for (int i = 0; i < 500000; i++) {
        add(n, 1);
    }

    t.join(); 
    std::cout << "n = " << n << std::endl;
}